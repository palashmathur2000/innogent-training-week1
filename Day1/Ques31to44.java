import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Ques31to44 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Organization> listOfEmployees = new ArrayList<>();

		Organization obj1 = new Organization(1, 25, 1000, 2017, 2000, "Palash", "Male", "IT");
		Organization obj2 = new Organization(2, 22, 2000, 2014, 1998, "Yash", "Male", "IT");
		Organization obj3 = new Organization(3, 23, 3000, 2016, 1945, "Aayush", "Male", "IT");
		Organization obj4 = new Organization(4, 24, 4000, 2020, 1990, "Ram", "Male", "CS");
		Organization obj5 = new Organization(5, 25, 5000, 2021, 2020, "Seeta", "Female", "IT");
		Organization obj6 = new Organization(6, 26, 2000, 2013, 2019, "geeta", "Female", "CS");

		listOfEmployees.add(obj1);
		listOfEmployees.add(obj2);
		listOfEmployees.add(obj3);
		listOfEmployees.add(obj4);
		listOfEmployees.add(obj5);
		listOfEmployees.add(obj6);

		int female = 0;
		int male = 0;

		int Fage = 0;
		int Mage = 0;

		int HeighestSalaryEmployeeId = 0;
		int HeighestSalaryEmployeeAge = 0;
		int HeighestSalaryEmployeeSalary = 0;
		String HeighestSalaryEmployeeName = "";
		String HeighestSalaryEmployeeGender = "";
		String HeighestSalaryEmployeeDepartment = "";

		int youngestMaleEmployeeId = 0;
		int youngestMaleEmployeeAge = 0;
		int youngestMaleEmployeeSalary = 0;
		String youngestMaleEmployeeName = "";
		String youngestMaleEmployeeGender = "";
		String youngestMaleEmployeeDepartment = "";

		int maximumsalary = Integer.MIN_VALUE;
		int min_age = Integer.MAX_VALUE;

		HashSet<String> departments = new HashSet<>();

		ArrayList<String> names = new ArrayList<>();

		HashMap<String, Integer> totalEmployee = new HashMap<>();
		HashMap<String, Integer> salaryMap = new HashMap<>();

		int fSalary = 0;
		int mSalary = 0;

		ArrayList<String> younger = new ArrayList<>();
		ArrayList<String> older = new ArrayList<>();

		int maximumage = Integer.MIN_VALUE;
		String Oldest_department = "";

		for (int i = 0; i < listOfEmployees.size(); i++) {
			Organization obj = listOfEmployees.get(i);

			// GENDER
			String gender = obj.getGender();

			if (gender.equals("Female")) {
				female++;
				Fage = Fage + obj.getAge();
				fSalary = fSalary + obj.getSalary();
			} else {
				male++;
				Mage = Mage + obj.getAge();
				mSalary = mSalary + obj.getSalary();
			}
			// DEPARMENTS
			departments.add(obj.getDepartment());

			int salary = obj.getSalary();

			if (salary > maximumsalary) {
				maximumsalary = salary;

				HeighestSalaryEmployeeId = obj.getId();
				HeighestSalaryEmployeeAge = obj.getAge();
				HeighestSalaryEmployeeSalary = salary;
				HeighestSalaryEmployeeName = obj.getName();
				HeighestSalaryEmployeeGender = obj.getGender();
				HeighestSalaryEmployeeDepartment = obj.getDepartment();
			}

			int DOJ = obj.getDOJ();
			if (DOJ > 2015) {
				names.add(obj.getName());
			}

			String Department = obj.getDepartment();

			if (totalEmployee.containsKey(Department)) {
				int count = totalEmployee.get(Department);
				totalEmployee.put(Department, ++count);
			} else {
				totalEmployee.put(Department, 1);
			}

			if (salaryMap.containsKey(Department)) {
				int sal = salaryMap.get(Department);
				salaryMap.put(Department, obj.getSalary() + sal);
			} else {
				salaryMap.put(Department, obj.getSalary());
			}

			if (obj.getDepartment().equals("IT") && obj.getGender().equals("Male") && obj.getAge() < min_age) {
				min_age = obj.getAge();

				youngestMaleEmployeeId = obj.getId();
				youngestMaleEmployeeAge = obj.getAge();
				youngestMaleEmployeeSalary = salary;
				youngestMaleEmployeeName = obj.getName();
				youngestMaleEmployeeGender = obj.getGender();
				youngestMaleEmployeeDepartment = obj.getDepartment();
			}

			int dob = obj.getDOB();

			if (2021 - dob < 25) {
				younger.add(obj.getName());
			} else {
				older.add(obj.getName());
				int age = 2021 - dob;
				if (age > maximumage) {
					maximumage = age;
				}

			}

			int age = obj.getAge();
			if (age > maximumage) {
				maximumage = age;

				Oldest_department = obj.getDepartment();
			}

			// question41
			System.out.println(obj.getName() + " " + obj.getDepartment());

		}

		System.out.println("female: " + female);
		System.out.println("male: " + male);

		System.out.println(departments);

		System.out.println("Female average age: " + Fage / female);
		System.out.println("Male average age: " + Mage / male);

		System.out.println("Heighest paid employee: " + HeighestSalaryEmployeeId + " " + HeighestSalaryEmployeeName
				+ " " + HeighestSalaryEmployeeAge + " " + " " + HeighestSalaryEmployeeDepartment + " "
				+ HeighestSalaryEmployeeGender + " " + HeighestSalaryEmployeeSalary);

		for (String name : names) {
			System.out.print(name + " ");
		}
		System.out.println();

		for (String department : totalEmployee.keySet()) {
			System.out.println(department + " = " + totalEmployee.get(department));
		}

		for (String department : salaryMap.keySet()) {
			System.out.println(
					department + " average salary = " + salaryMap.get(department) / totalEmployee.get(department));
		}

		System.out.println("youngest male employee in IT: " + youngestMaleEmployeeId + " " + youngestMaleEmployeeName
				+ " " + youngestMaleEmployeeAge + " " + " " + youngestMaleEmployeeDepartment + " "
				+ youngestMaleEmployeeGender + " " + youngestMaleEmployeeSalary);

		System.out.println("males Average Salary" + mSalary / male);

		System.out.println("females Average Salary" + fSalary / female);

		System.out.println("Total salary of organization: " + (mSalary + fSalary));
		System.out.println("Average salary of organization: " + (mSalary + fSalary) / (female + male));

		System.out.println("Younger ones");
		for (String younger_name : younger) {
			System.out.print(younger_name + " ");
		}
		System.out.println();
		System.out.println("older ones");

		for (String older_name : older) {
			System.out.print(older_name + " ");
		}

		System.out.println("Oldest employee: " + Oldest_department + " " + maximumage);
	}

}
