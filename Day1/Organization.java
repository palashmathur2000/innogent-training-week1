import java.util.Objects;

public class Organization 
{
	private int id;
	private int age;
	private int DOJ;
	private int salary;
	private int dob;
	private String name;
	private String gender;
	private String Department;

	
	public Organization(int id,int age,int salary,int DOJ,int dob , String name, String gender, String department)
	{
		this.id = id;
		this.age = age;
		this.name = name;
		this.gender = gender;
		this.Department = department;
		this.salary = salary;
		this.DOJ = DOJ;
		this.dob = dob;
	}

	public int getId() {
		return id;
	}
	
	public int getAge() {
		return age;
	}

	public int getSalary() {
		return salary;
	}

	public int getDOJ() {
		return DOJ;
	}
	
	public int getDOB() {
		return dob;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public String getDepartment() {
		return Department;
	}

	@Override
	public String toString() {
		return "Organization [id=" + id + ", age=" + age + ", DOJ=" + DOJ + ", salary=" + salary + ", dob=" + dob
				+ ", name=" + name + ", gender=" + gender + ", Department=" + Department + "]";
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
	
		Organization orgObj = (Organization)obj; 
		return this.getName().equals(orgObj.getName());
	
	}
	
	 
	
	
	
}
