public class Ques45 {

	public static void main(String[] args) {

		Student student1 = new Student(10, "Palash");
		Student student2 = new Student(10, "Palash");
		Student student3 = new Student(20, "Yash");

		System.out.println("student1: " + student1.hashCode());
		System.out.println("student2: " + student2.hashCode());
		System.out.println("student3: " + student3.hashCode());

		System.out.println(student1.equals(student2));

		System.out.println(student1.equals(student3));
	}

}
