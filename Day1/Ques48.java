class MyThread extends Thread {

	public void run() {
		for (int i = 1; i <= 5; i++) {
			try {
				System.out.println("MyThread class");
				Thread.sleep(500);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}

public class Ques48 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyThread t1 = new MyThread();
		t1.start();
		try {
			t1.join();
		} catch (Exception e) {
			System.out.println(e);
		}
		for (int i = 1; i <= 5; i++) {
			try {
				System.out.println("Main class");
				Thread.sleep(500);
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

}
