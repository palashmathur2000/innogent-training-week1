abstract class MyAbstractClass
{
	public abstract void show(); 
	
	public void run() 
	{
		System.out.println("Running");
	}
}

class MyClass extends MyAbstractClass
{
	public void show()
	{
		System.out.println("Show method");
	}
}


public class Ques15
{
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		MyClass obj = new MyClass();
		obj.show();
		obj.run();
		
	}
}
