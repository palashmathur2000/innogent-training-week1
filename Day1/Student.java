import java.io.Serializable;
class Student implements Cloneable, Serializable
{
	transient int studentAge;
	String studentName;
	
	Student(int studentAge, String studentName){
		this.studentAge = studentAge;
		this.studentName = studentName;
	}
	
	protected Object clone()throws CloneNotSupportedException 
	{
		return super.clone();
	}
	
}