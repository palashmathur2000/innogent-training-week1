import java.io.File;
import java.util.Date;

public class Ques23AND24AND25AND27AND29 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File file = new File("E:\\DYNAMIC PROGRAMMING.doc");

		// exits
		if (file.exists()) {
			System.out.println("exists YES");
		} else {
			System.out.println("exists NO");
		}

		// write
		if (file.canWrite()) {
			System.out.println("write yes");
		} else {
			System.out.println("write no");

		}

		// read
		if (file.canRead()) {
			System.out.println("read yes");
		} else {
			System.out.println("read no");

		}

		// Directory
		if (file.isDirectory()) {
			System.out.println("Directory yes");
		} else {
			System.out.println("Directory no");

		}

		// File
		if (file.isFile()) {
			System.out.println("File yes");
		} else {
			System.out.println("File no");

		}
		
		//Date
		Date d = new Date(file.lastModified());
		System.out.println(d);
		
		
		//SIZE IN KB, MB, Bytes
		
		System.out.println(file.length());
		System.out.println(file.length()/1024);
		System.out.println(file.length()/(1024*1024));
		
		
	}

}
