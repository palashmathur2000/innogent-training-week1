interface LivingBeings
{
	public abstract void walk();
	public abstract void eat();
	
}

class Human implements LivingBeings
{
	public void walk() 
	{
		System.out.println("Human is walking");
	}
	public void eat() 
	{
		System.out.println("Human is eating");
	}
}

class Animal implements LivingBeings
{
	public void walk() 
	{
		System.out.println("Animal is walking");
	}
	public void eat() 
	{
		System.out.println("Animal is eating");
	}
}

public class Ques14
{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Human palash = new Human();
		palash.walk();
		palash.eat();
		
		Animal dog = new Animal();
		dog.walk();
		dog.eat();
		
	}

}
