import java.util.HashMap;

public class Ques46 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Organization, String> org  = new HashMap<>();

		Organization obj1 = new Organization(1, 25, 1000, 2017, 2000, "Palash", "Male", "IT");
		Organization obj2 = new Organization(2, 22, 2000, 2014, 1998, "Yash", "Male", "IT");
		Organization obj3 = new Organization(2, 23, 3000, 2016, 1945, "Yash", "Male", "IT");
		Organization obj4 = new Organization(4, 24, 4000, 2020, 1990, "Ram", "Male", "CS");
		Organization obj5 = new Organization(5, 25, 5000, 2021, 2020, "Seeta", "Female", "IT");
		Organization obj6 = new Organization(6, 26, 2000, 2013, 2019, "geeta", "Female", "CS");

		org.put(obj1,"a");
		org.put(obj2,"b");
		org.put(obj3,"c");
		org.put(obj4,"d");
		org.put(obj5,"e");
		org.put(obj6,"f");
		
		System.out.println(org);
		
	}

}
