interface MyInterface
{
	public abstract void show();
	default void run()
	{
		System.out.println("Run");
	}
}
class Example implements MyInterface
{
	public void show() 
	{
		System.out.println("Showing");
	}
}

public class Ques16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Example ex = new Example();
		
		ex.run();
		ex.show();
	}

}
