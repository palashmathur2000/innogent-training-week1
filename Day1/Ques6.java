class First
{
	public static void fun() 
	{
		System.out.println("I M IN A");
	}
}
class Second extends First
{
	public static void fun() 
	{
		System.out.println("I M IN B");
	}
}

public class Ques6 {

	public static void main(String[] args) {
		
		First obj1 = new Second();
		
		obj1.fun();
		

		First obj2 = new First();
		
		obj2.fun();
		
		Second obj3= new Second();
		
		obj3.fun();
		

	}

}
