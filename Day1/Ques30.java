import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Ques30 {

	public static void main(String[] args)throws IOException
	{
		// TODO Auto-generated method stub
		File f = new File("E:\\DYNAMIC PROGRAMMING.doc");

		FileInputStream file = new FileInputStream(f); 
		
		byte arr[] = new byte[(int)f.length()];
		
		file.read(arr);
		
		for(byte data:arr) 
		{
			System.out.println(data);
		}
	}

}
