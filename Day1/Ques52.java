import java.util.ArrayList;
import java.util.Collections;

public class Ques52 {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		
		ArrayList<Employee> employeeList = new ArrayList<>();
		employeeList.add(new Employee(1,"Palash",300));
		employeeList.add(new Employee(2,"Yash",200));
		employeeList.add(new Employee(3,"Aayush",300));
		employeeList.add(new Employee(4,"Ram",300));
		employeeList.add(new Employee(5,"Seeta",200));
		
		Collections.sort(employeeList, new MyCamparator());
		
		System.out.println(employeeList);
		
	}

}




























