package com.Day3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ques11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

		System.out.println("enter a string");

		String word = sc.next();

		Map<Character, Integer> countOfCharInString = new HashMap<>();

		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			if (countOfCharInString.containsKey(c)) {
				int count = countOfCharInString.get(c);
				countOfCharInString.put(c, ++count);
			} else {
				countOfCharInString.put(c, 1);
			}
		}

		for (Character c : countOfCharInString.keySet()) {
			System.out.println(c + " = " + countOfCharInString.get(c));
		}
	}

}
