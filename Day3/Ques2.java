//FINAL KEYWORD EXAMPLE

package com.Day3;

interface Animal {
	public void run();
}

final class Dog implements Animal { // NO class can inherit it

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Dog is running");
	}

}

public class Ques2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final int x = 10; // Now we can not Change it

		Dog germanShefered = new Dog();
		germanShefered.run();
		System.out.println("final variable x value is :" + x);
	}

}
