package com.Day3;

import java.util.Scanner;

public class Ques8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter ur age");
		Scanner sc = new Scanner(System.in);
		int age = sc.nextInt();

		if (age < 18) {
			throw new ArithmeticException("You are not eligible");
		} else {
			System.out.println("You are eligible");
		}
	}

}
