package com.Day3;

enum Names {
	Palash, Yash, Aayush
}

public class Ques3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Names myNames = Names.Palash;
		System.out.println(myNames);

		myNames = Names.Yash;
		System.out.println(myNames);

		myNames = Names.Aayush;
		System.out.println(myNames);

		System.out.println("---------------------------");
		for(Names entry: Names.values()) 
		{
			System.out.println(entry);
		}
	}

}
