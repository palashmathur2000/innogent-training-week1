package com.Day3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

class MyExceptionClass extends RuntimeException {
	MyExceptionClass(String msg) {
		super(msg);
	}
}

public class Ques6 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		try {
			throw new MyExceptionClass("wrong input");
		} catch (Exception e) {
			System.out.println(e);
		}

		File file = new File("test.txt");
		FileInputStream f = new FileInputStream(file);

	}

}
