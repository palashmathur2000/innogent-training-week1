package com.day2.ExerciseSheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

public class Operations {

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		ArrayList<Section> sectionsList = new ArrayList<>();

		sectionsList.add(new Section(1, "A"));
		sectionsList.add(new Section(2, "B"));
		sectionsList.add(new Section(3, "C"));
		sectionsList.add(new Section(4, "D"));

		ArrayList<Student> studentsList = new ArrayList<>();

		studentsList.add(new Student(1, "stud1", "A", 88, "F", 10));
		studentsList.add(new Student(2, "stud2", "A", 70, "F", 11));
		studentsList.add(new Student(3, "stud3", "B", 88, "M", 22));
		studentsList.add(new Student(4, "stud4", "B", 55, "M", 33));
		studentsList.add(new Student(5, "stud5", "A", 30, "F", 44));
		studentsList.add(new Student(6, "stud6", "C", 30, "F", 33));
		studentsList.add(new Student(7, "stud7", "C", 10, "F", 22));
		studentsList.add(new Student(8, "stud8", "C", 110, "M", 11));

		ArrayList<Address> addressesList = new ArrayList<>();

		addressesList.add(new Address(1, 452002, "indore", 1));
		addressesList.add(new Address(2, 422002, "delhi", 1));
		addressesList.add(new Address(3, 442002, "indore", 2));
		addressesList.add(new Address(4, 462002, "delhi", 3));
		addressesList.add(new Address(5, 472002, "indore", 4));
		addressesList.add(new Address(6, 452002, "indore", 5));
		addressesList.add(new Address(7, 452002, "delhi", 5));
		addressesList.add(new Address(8, 482002, "mumbai", 6));
		addressesList.add(new Address(9, 482002, "bhopal", 7));
		addressesList.add(new Address(10, 482002, "indore", 8));

//----------------------------------------------------------------------------------------------------------------------------------	
		// Ques3, Ques4 and Ques5

		Collections.sort(studentsList);
		System.out.println("Rank 1: " + studentsList.get(0).getName() + " Marks: " + studentsList.get(0).getMarks());
		System.out.println("Rank 2: " + studentsList.get(1).getName() + " Marks: " + studentsList.get(1).getMarks());
		System.out.println("Rank 3: " + studentsList.get(2).getName() + " Marks: " + studentsList.get(2).getMarks());

		System.out.println("Passed students");
		for (int i = 0; i < studentsList.size(); i++) {
			Student st = studentsList.get(i);
			if (st.getMarks() > 50) {
				System.out.println(st);
			}
		}

		System.out.println("Failed Student");
		for (int i = 0; i < studentsList.size(); i++) {
			Student st = studentsList.get(i);
			if (st.getMarks() < 50) {
				System.out.println(st);
			}

		}

// ---------------------------------------------------------------------------------------------------
		// Ques1 AND Ques2 AND Ques6

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 1 if you want to filter the student based on pincode ");
		System.out.println("Enter 2 if you want to filter the student based on city ");
		System.out.println("Enter 3 if you want to filter the student based on studentID ");
		System.out.println("Enter 4 if you want to filter the student based on classID ");

		int n = sc.nextInt();

		HashSet<Student> filteredList = new HashSet<>();

		switch (n) {

		case 1:

			System.out.println("Enter the pincode ");
			int pin = sc.nextInt();

			for (int i = 0; i < addressesList.size(); i++) {
				Address adrObj = addressesList.get(i);

				if (pin == adrObj.getPinCode()) {
					int studentID = adrObj.getStudentID();

					for (int j = 0; j < studentsList.size(); j++) {
						Student stObj = studentsList.get(j);

						if (studentID == stObj.getId()) {
							filteredList.add(stObj);
						}
					}
				}
			}

			System.out.println(filteredList);
			sc.nextLine();

			break;

		case 2:

			System.out.println("Enter the city ");
			String city = sc.nextLine();

			for (int i = 0; i < addressesList.size(); i++) {
				Address adrObj = addressesList.get(i);

				if (city.equals(adrObj.getCity())) {
					int studentID = adrObj.getStudentID();

					for (int j = 0; j < studentsList.size(); j++) {
						Student stObj = studentsList.get(j);

						if (studentID == stObj.getId()) {
							filteredList.add(stObj);
						}
					}
				}
			}
			System.out.println(filteredList);

			break;

		case 3:
			sc.nextLine();
			HashSet<Student> filteredListOnGender = new HashSet<>();

			System.out.println("Enter the studentID ");
			String gender = sc.nextLine();

			for (int i = 0; i < studentsList.size(); i++) {
				Student stuObj = studentsList.get(i);

				if (gender.equals(stuObj.getGender())) {
					filteredListOnGender.add(stuObj);
				}
			}
			System.out.println(filteredListOnGender);
			break;

		case 4:
			sc.nextLine();
			System.out.println("Enter the classID ");
			String classID = sc.nextLine();

			for (int i = 0; i < studentsList.size(); i++) {
				Student stdObj = studentsList.get(i);

				if (classID.equals(stdObj.getClass_id())) {
					System.out.println(stdObj);
				}
			}

			break;

		}

	}
}
