package com.day2.ExerciseSheet;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class CSVMainFile {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		Scanner stdScanner = new Scanner(new File("E:\\Student.csv"));
		Scanner clsScanner = new Scanner(new File("E:\\Class.csv"));
		Scanner adrScanner = new Scanner(new File("E:\\Address.csv"));

		ArrayList<Student> stdList = new ArrayList<>();
		ArrayList<Section> clsList = new ArrayList<>();
		ArrayList<Address> adrList = new ArrayList<>();

// =========================================================================================================================
// QUES7
		while (stdScanner.hasNext()) {
			String stdData = stdScanner.nextLine();
			String stdInfo[] = stdData.split(",");

			Student st = new Student(Integer.parseInt(stdInfo[0]), stdInfo[1], stdInfo[2], Integer.parseInt(stdInfo[3]),
					stdInfo[4], Integer.parseInt(stdInfo[5]));
			stdList.add(st);
		}

		System.out.println(stdList);

		while (clsScanner.hasNext()) {
			String clsData = clsScanner.next();
			String clsInfo[] = clsData.split(",");

			clsList.add(new Section(Integer.parseInt(clsInfo[0]), clsInfo[1]));

		}

		System.out.println(clsList);

		while (adrScanner.hasNext()) {
			String adrData = adrScanner.next();
			String adrInfo[] = adrData.split(",");

			adrList.add(new Address(Integer.parseInt(adrInfo[0]), Integer.parseInt(adrInfo[1]), adrInfo[2],
					Integer.parseInt(adrInfo[3])));

		}

		System.out.println(adrList);

// =========================================================================================================================
// QUES8

		for (int i = 0; i < stdList.size(); i++) {

			Student stdObj = stdList.get(i);

			if (stdObj.getAge() > 20) {
				System.out.println(stdObj.getName() + " is failed");
			}
		}

// =========================================================================================================================
// QUES9

		Scanner sc = new Scanner(System.in);

		int stdID = sc.nextInt();

		for (int i = 0; i < stdList.size(); i++) {

			Student stdObj = stdList.get(i);

			if (stdObj.getId() == stdID) {
				stdList.remove(i);

				for (int j = 0; j < adrList.size(); j++) {

					Address adrObj = adrList.get(j);

					if (adrObj.getStudentID() == stdID) {
						adrList.remove(j);
					}
				}

			}
		}

// =========================================================================================================================
// QUES10

		HashMap<String, Integer> countOfStudentInClass = new HashMap<>();

		for (int i = 0; i < stdList.size(); i++) {

			Student stdObj = stdList.get(i);
			if (countOfStudentInClass.containsKey(stdObj.getClass_id())) {
				int classCount = countOfStudentInClass.get(stdObj.getClass_id());
				countOfStudentInClass.put(stdObj.getClass_id(), ++classCount);
			} else {
				countOfStudentInClass.put(stdObj.getClass_id(), 1);
			}

		}

		for (String s : countOfStudentInClass.keySet()) {
			System.out.println(s + " " + countOfStudentInClass.get(s));
		}

		System.out.println("Enter class Id u want to delete");
		while (true) {
			System.out.println("Enter class id u want to delete OR enter 'EXIT' alone to exit");

			String classID = sc.next();

			if (classID.equals("EXIT")) {
				break;
			}
			if (countOfStudentInClass.get(classID) == 1) {

				for (int i = 0; i < clsList.size(); i++) {
					Section clsObj = clsList.get(i);
					if (clsObj.getName().equals(classID)) {
						clsList.remove(i);
					}
				}

			} else {
				int classCount = countOfStudentInClass.get(classID);
				countOfStudentInClass.put(classID, --classCount);
			}

		}

		for (String s : countOfStudentInClass.keySet()) {
			System.out.println(s + " " + countOfStudentInClass.get(s));
		}

		for (Section cls : clsList) {
			System.out.println(cls.getId() + " " + cls.getName());
		}

// =========================================================================================================================
// QUES11

		System.out.println("Enter 1 to get female studends ORDER BY INDEX");
		System.out.println("Enter 2 to get female studends ORDER BY NAMES");
		System.out.println("Enter 3 to get female studends ORDER BY MARKS");

		int n = sc.nextInt();

		int start = 0;
		int end = 0;
		ArrayList<Student> filteredFemaleStdList = new ArrayList<Student>();
		switch (n) {

		case 1:

			System.out.println("gender");
			String gender = sc.next();
			System.out.println("start");
			start = sc.nextInt();
			System.out.println("end");
			end = sc.nextInt();

			filteredFemaleStdList = filterFemaleStudentOnGenderAndNumber(gender, start, end, stdList);

			for (Student st : filteredFemaleStdList) {
				System.out.println(st.getName() + " " + st.getGender());
			}

			break;

		case 2:

			System.out.println("start");
			start = sc.nextInt();
			System.out.println("end");
			end = sc.nextInt();

			filteredFemaleStdList = filterFemaleStudentOnNamesAndNumber(start, end, stdList);

			for (Student st : filteredFemaleStdList) {
				System.out.println(st.getName() + " " + st.getGender());
			}

			break;

		case 3:

			System.out.println("start");
			start = sc.nextInt();
			System.out.println("end");
			end = sc.nextInt();

			filteredFemaleStdList = filterFemaleStudentOnMarksAndNumber(start, end, stdList);

			for (Student st : filteredFemaleStdList) {
				System.out.println(st.getName() + " " + st.getMarks());
			}

			break;

		}

	}

	// TO FETCH FEMALE STUDENT ON THE BASIS OF INDEXES
	private static ArrayList<Student> filterFemaleStudentOnGenderAndNumber(String gender, int start, int end,
			ArrayList<Student> stdList) {

		ArrayList<Student> filteredFemaleStudentListOnGenderAndNumber = new ArrayList<Student>();

		ArrayList<Student> totalFemaleStudentList = getTotalFemaleStudent(stdList);

		if (start > totalFemaleStudentList.size() || end > totalFemaleStudentList.size()) {
			System.out.println("Invalid input");
		} else {

			int endCount = 0;
			for (int i = start - 1; i < totalFemaleStudentList.size(); i++) {

				filteredFemaleStudentListOnGenderAndNumber.add(totalFemaleStudentList.get(i));
				endCount++;

				if (endCount >= end) {
					break;
				}

			}
		}

		return filteredFemaleStudentListOnGenderAndNumber;
	}

	// TO FETCH FEMALE STUDENT ON THE BASIS OF NAMES AND INDEXES BOTH
	private static ArrayList<Student> filterFemaleStudentOnNamesAndNumber(int start, int end,
			ArrayList<Student> stdList) {

		ArrayList<Student> filteredFemaleStudentListOnNamesAndNumber = new ArrayList<Student>();

		ArrayList<Student> totalFemaleStudentList = getTotalFemaleStudent(stdList);

		Collections.sort(totalFemaleStudentList);

		if (start > totalFemaleStudentList.size() || end > totalFemaleStudentList.size()) {
			System.out.println("Invalid input");
		} else {

			int endCount = 0;
			for (int i = start - 1; i < totalFemaleStudentList.size(); i++) {

				filteredFemaleStudentListOnNamesAndNumber.add(totalFemaleStudentList.get(i));
				endCount++;

				if (endCount >= end) {
					break;
				}

			}
		}

		return filteredFemaleStudentListOnNamesAndNumber;
	}

	// TO FETCH FEMALE STUDENT ON THE BASIS OF MARKS
	private static ArrayList<Student> filterFemaleStudentOnMarksAndNumber(int start, int end,
			ArrayList<Student> stdList) {

		ArrayList<Student> filteredFemaleStudentListOnMarksAndNumber = new ArrayList<Student>();

		ArrayList<Student> totalFemaleStudentList = getTotalFemaleStudent(stdList);

		Collections.sort(totalFemaleStudentList);

		if (start > totalFemaleStudentList.size() || end > totalFemaleStudentList.size()) {
			System.out.println("Invalid input");
		} else {

			int endCount = 0;
			for (int i = start - 1; i < totalFemaleStudentList.size(); i++) {

				filteredFemaleStudentListOnMarksAndNumber.add(totalFemaleStudentList.get(i));
				endCount++;

				if (endCount >= end) {
					break;
				}

			}
		}

		return filteredFemaleStudentListOnMarksAndNumber;
	}

	// TO FETCH ALL FEMALE STUDENT
	private static ArrayList<Student> getTotalFemaleStudent(ArrayList<Student> stdList) {

		ArrayList<Student> totalFemaleStudentList = new ArrayList<Student>();

		for (Student st : stdList) {
			if (st.getGender().equals("F")) {
				totalFemaleStudentList.add(st);
			}
		}

		return totalFemaleStudentList;
	}

}
