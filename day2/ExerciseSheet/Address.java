package com.day2.ExerciseSheet;

public class Address {

	private int id;
	private int pinCode;
	private String city;
	private int studentID;

	public Address(int id, int pinCode, String city, int studentID) {
		super();
		this.id = id;
		this.pinCode = pinCode;
		this.city = city;
		this.studentID = studentID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", pinCode=" + pinCode + ", city=" + city + ", studentID=" + studentID + "]";
	}
	
	

}
