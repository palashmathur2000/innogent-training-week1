package com.day2.ExerciseSheet;

public class Student implements Comparable<Student> {

	private int id;
	private String name;
	private String class_id;
	private int marks;
	private String gender;
	private int age;

	public Student(int id, String name, String class_id, int marks, String gender, int age) {
		super();
		this.id = id;
		this.name = name;
		this.class_id = class_id;
		this.marks = marks;
		this.gender = gender;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClass_id() {
		return class_id;
	}

	public void setClass_id(String class_id) {
		this.class_id = class_id;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", class_id=" + class_id + ", marks=" + marks + ", gender="
				+ gender + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Student o) {
		// TODO Auto-generated method stub

		return o.getMarks() - this.getMarks(); // (FOR QUESTION NO.3 AND 10(3))
		// return this.getName().compareTo(o.getName());
	}

}
