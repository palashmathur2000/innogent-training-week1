package com.day2.ExerciseSheet;

import java.util.ArrayList;

public class Section {

	private int id;
	private String name;

	public Section(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Section [id=" + id + ", name=" + name + "]";
	}

}
